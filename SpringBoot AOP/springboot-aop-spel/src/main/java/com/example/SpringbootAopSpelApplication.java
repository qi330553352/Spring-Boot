package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootAopSpelApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootAopSpelApplication.class, args);
	}

}
