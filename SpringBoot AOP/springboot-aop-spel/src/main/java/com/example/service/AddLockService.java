/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.service;

/**
 * 创  建   时  间： 2020/6/27
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
public interface AddLockService {
    /**
     * 加锁
     * @param redisKey key
     * @param i i秒之后失效
     * @return 1 成功 -1 失败 -2 失败
     */
    int addLock(String redisKey, int i);

    /**
     * 清除锁
     * @param redisKey key
     */
    void clearLock(String redisKey);
}
