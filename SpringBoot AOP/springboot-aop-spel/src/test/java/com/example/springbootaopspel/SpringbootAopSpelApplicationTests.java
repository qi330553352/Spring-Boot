package com.example.springbootaopspel;

import com.example.annotation.AddLock;
import com.example.entity.Order;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringbootAopSpelApplicationTests {

	@Test
	void contextLoads() {
	}

	@AddLock(spel = "'CreateOutOrder'+#p0.orderCode",logInfo = "日志信息")
	public void testAddLock(Order order){

	}

	@AddLock(spel = "'CreateOutOrder'+#order.orderCode",logInfo = "日志信息")
	public void testAddLock1(Order order){

	}
}
