package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * https://www.jianshu.com/p/bca733826e2e
 */
@SpringBootApplication
public class SpringsecurityOauth2JwtApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringsecurityOauth2JwtApplication.class, args);
	}

}
