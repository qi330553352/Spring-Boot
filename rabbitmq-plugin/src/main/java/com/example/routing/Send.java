package com.example.routing;

import com.example.utils.ConnectionUtil;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

/** 路由模式（routing）-生产者
 * 创 建 时 间: 2019/10/13
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public class Send {

    private static final String EXCHANGE_NAME = "exchange_name_direct";

    public static void main(String[] args) throws Exception {
        Connection conn = ConnectionUtil.getConnection();
        Channel channel = conn.createChannel();// 创建一个通道
        channel.exchangeDeclare(EXCHANGE_NAME,"direct");// 声明交换机，分发

        String msg = "hello, diret msg...";
        String routingKey = "error"; // 路由键
        channel.basicPublish(EXCHANGE_NAME,routingKey,null,msg.getBytes());// 发送消息

        channel.close();
        conn.close();
    }

}
