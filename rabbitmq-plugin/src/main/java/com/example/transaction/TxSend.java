package com.example.transaction;

import com.example.utils.ConnectionUtil;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

/** AMQP实现了事务机制
 * 创 建 时 间: 2019/10/13
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public class TxSend {


    private static final String QUEUE_NAME = "simple_queue";

    public static void main(String[] args) throws Exception {
        Connection conn = ConnectionUtil.getConnection();
        Channel channel = conn.createChannel();// 创建一个通道
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);// 创建队列声明
        String msg = "hello, simple msg...";
        try{
            channel.txSelect(); // 将当前channel设置成Transaction模式
            channel.basicPublish("",QUEUE_NAME,null,msg.getBytes());// 发送消息
            channel.txCommit();
        } catch (Exception e) {
            e.printStackTrace();
            channel.txRollback();
        }
        channel.close();
        conn.close();
    }
}
