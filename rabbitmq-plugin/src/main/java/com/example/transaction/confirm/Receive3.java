package com.example.transaction.confirm;

import com.example.utils.ConnectionUtil;
import com.rabbitmq.client.*;

import java.io.IOException;

/** confirm确认机制-异步的 confirm模式：提供一个回调方法
 * 创 建 时 间: 2019/10/13
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public class Receive3 {

    private static final String QUEUE_NAME = "simple_queue";

    public static void main(String[] args) throws Exception {
        Connection conn = ConnectionUtil.getConnection();
        Channel channel = conn.createChannel();// 创建一个通道
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);// 创建队列声明
        channel.basicConsume(QUEUE_NAME,true,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String msg = new String(body,"UTF-8");
                System.out.println("confirm确认机制-异步的 confirm模式：提供一个回调方法:"+msg);
            }
        });
    }

}
