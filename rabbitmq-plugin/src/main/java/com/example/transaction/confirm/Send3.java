package com.example.transaction.confirm;

import com.example.utils.ConnectionUtil;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConfirmListener;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;

/** confirm确认机制-异步的 confirm模式：提供一个回调方法
 * 创 建 时 间: 2019/10/13
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public class Send3 {

    private static final String QUEUE_NAME = "simple_queue";

    public static void main(String[] args) throws Exception {
        Connection conn = ConnectionUtil.getConnection();
        Channel channel = conn.createChannel();// 创建一个通道
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);// 创建队列声明

        channel.confirmSelect(); // 注意：queue必须是一个新的
        final SortedSet<Long> confirmSet = Collections.synchronizedSortedSet(new TreeSet<>());
        channel.addConfirmListener(new ConfirmListener() {
            @Override
            public void handleNack(long seq, boolean multiple) throws IOException {
                if(multiple){
                    System.out.println("---handleNack-------multiple:true-----");
                    confirmSet.headSet(seq+1).clear();
                }else{
                    System.out.println("---handleNack-------multiple:false-----");
                    confirmSet.remove(seq);
                }
            }

            @Override//没有问题的
            public void handleAck(long seq, boolean multiple) throws IOException {
                if(multiple){
                    System.out.println("---handleAck-------multiple:true-----");
                    confirmSet.headSet(seq+1).clear();
                }else{
                    System.out.println("---handleAck-------multiple:false-----");
                    confirmSet.remove(seq);
                }
            }
        });
        while (true){
            long seqNo = channel.getNextPublishSeqNo();
            channel.basicPublish("",QUEUE_NAME,null,"seccess".getBytes());
            confirmSet.add(seqNo);
        }

    }

}
