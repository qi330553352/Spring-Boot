package com.example.transaction.confirm;

import com.example.utils.ConnectionUtil;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

/** confirm确认机制-普通模式 发一条 waitForConfirms()
 * 创 建 时 间: 2019/10/13
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public class Send1 {

    private static final String QUEUE_NAME = "simple_queue";

    public static void main(String[] args) throws Exception {
        Connection conn = ConnectionUtil.getConnection();
        Channel channel = conn.createChannel();// 创建一个通道
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);// 创建队列声明

        channel.confirmSelect(); // 注意：queue必须是一个新的
        String msg = "hello, simple msg...";
        channel.basicPublish("",QUEUE_NAME,null,msg.getBytes());// 发送消息
        if(channel.waitForConfirms()) {
            System.out.println("发送成功");
        }else{
            System.out.println("发送失败");
        }

        channel.close();
        conn.close();
    }

}
