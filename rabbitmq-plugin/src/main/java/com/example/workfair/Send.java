package com.example.workfair;

import com.example.utils.ConnectionUtil;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

/** 工作队列-公平分发-生产者
 * 创 建 时 间: 2019/10/13
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public class Send {

    private static final String QUEUE_NAME = "work_queue";

    public static void main(String[] args) throws Exception {
        Connection conn = ConnectionUtil.getConnection();
        Channel channel = conn.createChannel();// 创建一个通道
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);// 创建队列声明

        /**
         * 消费者在未发送确认消息到生产者之前，生产者每次只发送1个消息到对应的消费者
         * （限制发送给同一个消费者不得超过一条消息）
         */
        channel.basicQos(1);
        for(int i=0;i<50;i++) {
            String msg = "hello, work "+i+" msg...";
            channel.basicPublish("",QUEUE_NAME,null,msg.getBytes());// 发送消息
        }

        channel.close();
        conn.close();
    }

}
