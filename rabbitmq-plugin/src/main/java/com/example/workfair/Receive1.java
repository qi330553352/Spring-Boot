package com.example.workfair;

import com.example.utils.ConnectionUtil;
import com.rabbitmq.client.*;

import java.io.IOException;

/** 工作队列-公平分发-消费者1
 * 创 建 时 间: 2019/10/13
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public class Receive1 {

    private static final String QUEUE_NAME = "work_queue";

    public static void main(String[] args) throws Exception {
        Connection conn = ConnectionUtil.getConnection();
        Channel channel = conn.createChannel();// 创建一个通道
        boolean durable = false; // 消息持久化
        channel.queueDeclare(QUEUE_NAME,durable,false,false,null);// 创建队列声明
        channel.basicQos(1);
        DefaultConsumer consumer = new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String msg = new String(body,"UTF-8");
                System.out.println("消费者【1】接收到消息:"+msg);
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                channel.basicAck(envelope.getDeliveryTag(),false);
            }
        };
        boolean autoAck = false; // 自动应答
        channel.basicConsume(QUEUE_NAME,autoAck,consumer); // 监听队列
    }

}
