/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.entity;
import com.example.annotation.DateValidator;
import com.example.controller.AccountService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;
/**
 * 创  建   时  间： 2020/6/27
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account {
    private String id;
    @NotNull
    @Length(max = 20)
    private String userName;
    @NotNull(groups = {AccountService.class})
    @Pattern(regexp = "[A-Z][a-z][0-9]")
    private String passWord;
    @DateTimeFormat(pattern = "yyy-MM-dd")
    private Date createTime;
    private String alias;
    @Max(10)
    @Min(1)
    private Integer level;
    private Integer vip;

    @DateValidator(dateFormat = "yyyy-MM-dd",groups = {AccountService.class})
    private String birthday;
    public Account(String id,String userName,String passWord,Date createTime,String alias,Integer level,Integer vip){

    }
}
