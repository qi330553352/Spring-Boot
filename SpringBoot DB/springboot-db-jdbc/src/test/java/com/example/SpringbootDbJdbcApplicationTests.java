package com.example;

import com.example.entity.UserInfo;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;

@SpringBootTest
class SpringbootDbJdbcApplicationTests {

	@Test
	void contextLoads() throws IOException {
		UserInfo user = new UserInfo();
		ClassPathResource res = new ClassPathResource("temp.jpg");
		byte[] mockImg = FileCopyUtils.copyToByteArray(res.getFile());
		user.setPostAttach(mockImg);
		user.setPostTest("这是大字段");
	}

}
