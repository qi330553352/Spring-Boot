/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.dao;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 查询单值数据
 *
 * 创  建   时  间： 2020/5/23
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
public class SimpleQueryExample extends JdbcDaoSupport {
    public int getForumNum(){
        String sql = "select count(*) from t_user ";
        return getJdbcTemplate().queryForObject(sql,Integer.class);
    }

    public double getReplyRate(int userId){
        String sql = "select topic_replies,topic_views from t_topic where user_id = ?";
        Object obj = getJdbcTemplate().queryForObject(sql, new Object[]{userId}, new RowMapper<Object>() {
            @Override
            public Object mapRow(ResultSet rs, int idx) throws SQLException {
                return new Double(0.0);
            }
        });
        return ((Double)obj).doubleValue();
    }
}
