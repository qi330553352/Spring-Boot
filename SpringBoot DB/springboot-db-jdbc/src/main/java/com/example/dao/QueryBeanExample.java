/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.dao;

import com.example.entity.UserInfo;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * 查询数据
 *
 * 创  建   时  间： 2020/5/23
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
public class QueryBeanExample extends JdbcDaoSupport {
    public void getForum1(final int forumId){
        String sql = "";
        getJdbcTemplate().query(sql, new Object[]{forumId}, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
                rs.getString("name");
                rs.getInt("age");
            }
        });
    }
    public List<UserInfo> getForum2(final int forumId){
        String sql = "";
        return getJdbcTemplate().query(sql, new Object[]{forumId}, new RowMapper<UserInfo>() {
            @Override
            public UserInfo mapRow(ResultSet rs, int idx) throws SQLException {
                UserInfo user = new UserInfo();
                rs.getInt("age");
                return user;
            }
        });
    }
}
