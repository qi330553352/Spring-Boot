/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.dao;

import com.example.entity.UserInfo;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 * 使用命名参数绑定的模板类
 *
 * 创  建   时  间： 2020/5/23
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
public class NamedParameterExample extends NamedParameterJdbcDaoSupport{
    public void addForum(final UserInfo user){
        final String sql = "insert into ... values (:forumName,:forumDesc);"; // 定义命名参数
        SqlParameterSource sps = new BeanPropertySqlParameterSource(user); // 定义参数源
        getNamedParameterJdbcTemplate().update(sql,sps); // 使用模板方法
        // -------------------------------------
        MapSqlParameterSource map = new MapSqlParameterSource().addValue("forumName",user.getPostAttach());
    }
}
