/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.dao;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;


/**
 * 创  建   时  间： 2020/5/23
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
public class ForumJdbcDao extends JdbcDaoSupport {

    public void initDb() {
        String sql = "create table t_user(id int primary key,name varchar(255)) ";
        getJdbcTemplate().execute(sql);
    }

    public void addForum() {
        String sql = "";
        List<String> params = new ArrayList<>();
        getJdbcTemplate().update(sql, params, new int[]{Types.VARCHAR, Types.VARCHAR});
        getJdbcTemplate().update(sql, new PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps) throws SQLException {
                ps.setString(1, "");
                ps.setInt(2, 999);
            }
        });
        getJdbcTemplate().update(sql, new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setString(1, "");
                ps.setInt(2, 999);
                return ps;
            }
        });
        getJdbcTemplate().queryForRowSet(sql); // 以行集返回数据
    }

    /**
     * 返回新增记录时的自增长主键值
     *
     * @return 自增长主键值
     */
    public int addForum2() {
        String sql = "";
        List<String> params = new ArrayList<>();
        KeyHolder keyHolder = new GeneratedKeyHolder(); // 主键持有者
        getJdbcTemplate().update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setString(1, "999");
                ps.setInt(2, 999);
                return ps;
            }
        }, keyHolder);
        return keyHolder.getKey().intValue();
    }
}
