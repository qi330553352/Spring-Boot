/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.dao;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * 批量更新示例
 * <p>
 * 创  建   时  间： 2020/5/23
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
public class BatchUpdateExample extends JdbcDaoSupport {
    /**
     * 批量操作
     *
     * @param list 数据
     */
    public void addForums(final List<String> list) {
        final String sql = "";
        getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int idx) throws SQLException {
                ps.setString(1, list.get(idx));
            }

            @Override
            public int getBatchSize() {
                return list.size(); // 批量更改的记录数
            }
        });
    }
}
