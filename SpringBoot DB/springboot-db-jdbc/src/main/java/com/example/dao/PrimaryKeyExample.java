/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.dao;

import com.example.entity.UserInfo;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.support.AbstractLobCreatingPreparedStatementCallback;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.incrementer.DataFieldMaxValueIncrementer;
import org.springframework.jdbc.support.incrementer.MySQLMaxValueIncrementer;
import org.springframework.jdbc.support.incrementer.OracleSequenceMaxValueIncrementer;
import org.springframework.jdbc.support.lob.LobCreator;
import org.springframework.jdbc.support.lob.LobHandler;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * 自增键和行集
 * OracleSequenceMaxValueIncrementer
 * MySQLMaxValueIncrementer
 * 创  建   时  间： 2020/5/23
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
public class PrimaryKeyExample extends JdbcDaoSupport {
    private DataFieldMaxValueIncrementer incre; // 主键值产生器

    private LobHandler lobHandler;

    public void addPost(final UserInfo user){
        String sql = "insert into table ...";
        getJdbcTemplate().execute(sql, new AbstractLobCreatingPreparedStatementCallback(this.lobHandler) {
            @Override
            protected void setValues(PreparedStatement ps, LobCreator lobCreator) throws SQLException, DataAccessException {
                ps.setInt(1,incre.nextIntValue()); // 获取下一个主键值
            }
        });
    }

    /*  创建序列

        create sequence seq_post_id
        increment by 1
        start with 1;


     */

    public DataFieldMaxValueIncrementer getIncre() {
        return incre;
    }

    public void setIncre(DataFieldMaxValueIncrementer incre) {
        this.incre = incre;
    }

    public LobHandler getLobHandler() {
        return lobHandler;
    }

    public void setLobHandler(LobHandler lobHandler) {
        this.lobHandler = lobHandler;
    }
}
