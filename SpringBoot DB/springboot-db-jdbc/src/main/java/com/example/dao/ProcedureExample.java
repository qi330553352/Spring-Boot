/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.dao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.CallableStatementCreatorFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

/**
 * 调用存储过程
 *
 * 创  建   时  间： 2020/5/23
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
public class ProcedureExample extends JdbcDaoSupport {
    /* 创建一个存储过程

    delimiter //    将语句的结束符调整为//，否则存储过程中的；语句结束符会被错误解析
        CREATE PROCEDURE P_GET_TOPIC_NUM（IN in_user_id INT,OUT out_num INT）
        BEGIN
            SELECT COUNT(*) INTO out_num FROM t_topic WHERE user_id = in_user_id;
        END
        //
    delimiter ;     重新将语句结束符调整为；

     */

    public int getUserTopicNum1(final int userId){
        String sql = "{call P_GET_TOPIC_NUM(?,?)}"; // 调用存储过程的sql语句
        Object obj = getJdbcTemplate().execute(sql, new CallableStatementCallback<Object>() {
            @Override
            public Object doInCallableStatement(CallableStatement cs) throws SQLException, DataAccessException {
                cs.setInt(1,userId); // 绑定入参
                cs.registerOutParameter(2, Types.INTEGER); // 注册输出参数
                cs.execute();
                return new Integer(cs.getInt(2)); // 获取输出参数的值
            }
        });
        return ((Integer)obj).intValue();
    }

    public int getUserTopicNum2(final int userId){
        String sql = "{call P_GET_TOPIC_NUM(?,?)}"; // 调用存储过程的sql语句
        CallableStatementCreatorFactory fac = new CallableStatementCreatorFactory(sql);
        fac.addParameter(new SqlParameter("userId",Types.INTEGER));
        fac.addParameter(new SqlOutParameter("topicNum",Types.INTEGER));
        Map paramsMap = new HashMap();
        paramsMap.put("userId",userId);
        CallableStatementCreator csc = fac.newCallableStatementCreator(paramsMap);
        Object obj = getJdbcTemplate().execute(csc, new CallableStatementCallback<Object>() {
            @Override
            public Object doInCallableStatement(CallableStatement cs) throws SQLException, DataAccessException {
                cs.execute();
                return new Integer(cs.getInt(2));
            }
        });
        return ((Integer)obj).intValue();
    }
}
