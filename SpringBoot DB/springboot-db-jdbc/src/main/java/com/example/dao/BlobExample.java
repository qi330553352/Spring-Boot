/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.dao;

import com.example.entity.UserInfo;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.AbstractLobCreatingPreparedStatementCallback;
import org.springframework.jdbc.core.support.AbstractLobStreamingResultSetExtractor;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.lob.LobCreator;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * BLOB/CBOB 类型数据的操作
 *
 * 创  建   时  间： 2020/5/23
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
public class BlobExample extends JdbcDaoSupport {
    private LobHandler lobHandler;

    public LobHandler getLobHandler() {
        return lobHandler;
    }

    public void setLobHandler(LobHandler lobHandler) {
        this.lobHandler = lobHandler;
    }

    /**
     * 存BLOB/CBOB
     *
     * @param user 对象
     */
    public void addPost(final UserInfo user){
        final String sql = "INSERT INTO t_user_info ...";
        getJdbcTemplate().execute(sql, new AbstractLobCreatingPreparedStatementCallback(this.lobHandler) {
            @Override
            protected void setValues(PreparedStatement ps, LobCreator lobCreator) throws SQLException, DataAccessException {
                ps.setInt(1,1);
                ps.setInt(2,user.getId());
                lobCreator.setClobAsString(ps,3,user.getPostTest()); // 设置Clob字段
                lobCreator.setBlobAsBytes(ps,4,user.getPostAttach()); // 设置Blob字段
            }
        });
    }

    /**
     * 取BLOB/CBOB
     */
    public void getPost(final int userId){
        String sql = "";
        getJdbcTemplate().query(sql, new Object[]{userId}, new RowMapper<UserInfo>() {
            @Override
            public UserInfo mapRow(ResultSet rs, int idx) throws SQLException {
                int postId =rs.getInt(1);
                byte[] attach = lobHandler.getBlobAsBytes(rs,2); // 以二进制数组方式获取BLOB数据
                UserInfo user = new UserInfo();
                user.setPostAttach(attach);
                return user;
            }
        });
    }

    /**
     * 以流数据方式读取LOB数据
     * @param postId id
     * @param os 用于接收LOB数据的输出流
     */
    public void getAttach(final int postId, final OutputStream os){
        String sql = "select post_attach from t_post ...";
        getJdbcTemplate().query(sql, new Object[]{postId}, new AbstractLobStreamingResultSetExtractor<Object>() {
            @Override // 没找到数据的情况
            protected void handleNoRowFound() throws DataAccessException {
                System.out.println("not found result!");
            }

            @Override
            protected void handleMultipleRowsFound() throws DataAccessException {
                super.handleMultipleRowsFound();
            }

            @Override
            protected void streamData(ResultSet rs) throws SQLException, IOException, DataAccessException {
                InputStream ins = lobHandler.getBlobAsBinaryStream(rs,1);
                if(ins!=null){
                    FileCopyUtils.copy(ins,os);
                }
            }
        });
    }

}
