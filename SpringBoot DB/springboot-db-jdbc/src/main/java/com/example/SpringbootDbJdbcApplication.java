package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootDbJdbcApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootDbJdbcApplication.class, args);
	}

}
