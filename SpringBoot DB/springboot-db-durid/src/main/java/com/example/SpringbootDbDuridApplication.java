package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * https://github.com/alibaba/druid
 *
 * 创  建   时  间： 2020/6/27
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
@SpringBootApplication
public class SpringbootDbDuridApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootDbDuridApplication.class, args);
	}

}
