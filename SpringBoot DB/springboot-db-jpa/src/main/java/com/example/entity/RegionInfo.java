/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.entity;

import lombok.Data;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import java.util.Date;

/**
 * 创  建   时  间： 2020/5/23
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
@Data
@Entity(name="region_info_t")
public class RegionInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name",length = 100)
    private String name;
    @Column(name = "create_time")
    @Temporal(TemporalType.DATE)
    private Date createTime;
    @Lob
    @Basic(fetch = FetchType.EAGER) // 不采用延迟加载机制
    @Column(name = "post_text",columnDefinition = "longtext not null")
    private String postText;
    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "post_attach",columnDefinition = "BLOB")
    private byte[] postAttach;
    @Transient
    private boolean tempProp;
}
