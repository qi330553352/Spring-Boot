/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.configs;

import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.cache.guava.GuavaCacheManager;
import org.springframework.cache.support.CompositeCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.cache.DefaultRedisCachePrefix;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 创  建   时  间： 2020/6/27
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
@EnableCaching // 启用Spring Cache
@SpringBootConfiguration
public class CacheConfiguration {
    // 注释掉的 是我自己 分别配置几种缓存的时候用的 spring ioc 中只能有一个 CacheManager 实列，如果 有多个会报错。
    //@Bean
//    public CacheManager guavaCacheManager() {
//        GuavaCacheManager cacheManager = new GuavaCacheManager();
//        cacheManager.setCacheBuilder(CacheBuilder.newBuilder().expireAfterWrite(3600, TimeUnit.SECONDS).maximumSize(1000));
//        return cacheManager;
//    }
//
//    @Bean
//    public CacheManager cacheManager(RedisTemplate<Object, Object> redisTemplate) {
//        RedisCacheManager redisCacheManager = new RedisCacheManager(redisTemplate);
//        return redisCacheManager;
//    }
    @Value("${spring.cache.ehcache.config}")
    private String ehCacheCongifPath;

    /**
     * @return
     */
    @Bean
    public EhCacheManagerFactoryBean ehCacheManagerFactoryBean() {
        EhCacheManagerFactoryBean cacheManagerFactoryBean = new EhCacheManagerFactoryBean();
        System.out.println(ehCacheCongifPath);
        cacheManagerFactoryBean.setConfigLocation(new ClassPathResource(ehCacheCongifPath));
        cacheManagerFactoryBean.setShared(true);
        //如果 Factory 自己手动实列化，需要 执行afterPropertiesSet()方法，因为这是方法是 初始化 类使用的
        //如果Factory 由Spring 容器 创建 ，容器初始化完成后 spring 会去执行这个方法。
//        cacheManagerFactoryBean.afterPropertiesSet();//初始化 读取配置文件,

        return cacheManagerFactoryBean;
    }

    /**
     * 混合缓存管理
     *
     * @param redisTemplate redis template
     * @return cacheManager
     */
    @Bean
    public CacheManager compositeCacheManager(@Autowired RedisTemplate<Object, Object> redisTemplate, @Autowired EhCacheManagerFactoryBean factoryBean) {
        RedisCacheManager redisCacheManager = getRedisCacheManager(redisTemplate);
        GuavaCacheManager guavaCacheManager = getGuavaCacheManager();
        EhCacheCacheManager ehCacheCacheManager = ehCacheCacheManager(factoryBean);
        CompositeCacheManager cacheManager = new CompositeCacheManager(redisCacheManager, guavaCacheManager, ehCacheCacheManager);
        cacheManager.setFallbackToNoOpCache(true);
        cacheManager.afterPropertiesSet();
        return cacheManager;
    }

    /**
     * 获取guava 实列的缓存
     *
     * @return guava缓存管理 实列
     */
    private GuavaCacheManager getGuavaCacheManager() {
        GuavaCacheManager guavaCacheManager = new GuavaCacheManager();
        guavaCacheManager.setCacheBuilder(CacheBuilder.newBuilder().expireAfterWrite(3600, TimeUnit.SECONDS).maximumSize(1000));
        ArrayList<String> guavaCacheNames = Lists.newArrayList();
        guavaCacheNames.add("GUAVA_CACHE_A");
        guavaCacheManager.setCacheNames(guavaCacheNames);
        return guavaCacheManager;
    }

    /**
     * 获取redisCacheManager
     *
     * @param redisTemplate redisTemplate
     * @return redisCacheManager
     */
    private RedisCacheManager getRedisCacheManager(RedisTemplate<Object, Object> redisTemplate) {
        RedisCacheManager redisCacheManager = new RedisCacheManager(redisTemplate);
        List<String> redisCacheNames = Lists.newArrayList();
        redisCacheNames.add("GUAVA_CACHE_A");//一个cacheName 对应一个 缓存实列
        redisCacheNames.add("REDIS_CACHE_B");
        redisCacheManager.setCacheNames(redisCacheNames);
        //redis key 前缀
        redisCacheManager.setCachePrefix(new DefaultRedisCachePrefix("demo"));//缓存key 前缀
        redisCacheManager.setUsePrefix(true);//使用前缀
        redisCacheManager.initializeCaches();//rediscache 需要初始化 缓存
        return redisCacheManager;
    }


    /**
     * EhCacheManager
     *
     * @return EhCacheManager
     */
    private EhCacheCacheManager ehCacheCacheManager(EhCacheManagerFactoryBean factoryBean) {
        EhCacheCacheManager ehCacheCacheManager = new EhCacheCacheManager(factoryBean.getObject());
        //由于自己实列化EhCacheManager 需要执行 手动初始化 方法。
        ehCacheCacheManager.initializeCaches();//初始化
        return ehCacheCacheManager;
    }
}
