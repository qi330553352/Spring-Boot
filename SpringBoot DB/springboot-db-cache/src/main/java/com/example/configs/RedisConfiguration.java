/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.configs;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.data.redis.support.collections.RedisProperties;
import redis.clients.jedis.JedisPoolConfig;

/**
 * 创  建   时  间： 2020/6/27
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
@EnableCaching // 启用Spring Cache
@SpringBootConfiguration
public class RedisConfiguration extends CachingConfigurerSupport {
    @Bean
    @ConfigurationProperties(prefix="spring.redis")
    public JedisPoolConfig getRedisConfig(){
        JedisPoolConfig config = new JedisPoolConfig();
        return config;
    }



    @Bean
    public JedisConnectionFactory getConnectionFactory(RedisProperties redisProperties){
        JedisConnectionFactory factory = new JedisConnectionFactory();
        return factory;
    }


    @Bean
    public StringRedisTemplate getRedisTemplate(RedisProperties redisProperties){
        JedisConnectionFactory connectionFactory = getConnectionFactory(redisProperties);
        StringRedisTemplate template = new StringRedisTemplate(connectionFactory);
        //设置redis 序列化
        template.setStringSerializer(new StringRedisSerializer());
        return template;
    }
}
