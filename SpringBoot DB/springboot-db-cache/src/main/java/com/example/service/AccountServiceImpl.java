/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.service;

import com.example.entity.Account;
import com.google.common.collect.Lists;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 创  建   时  间： 2020/6/27
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
@Service
public class AccountServiceImpl {
    @CachePut(value = "EHCACHE_A",key = "#account.id")
    public Account saveAccount(Account account) {

        System.out.println("保存成功"+account);
        account.setId("999");
        account.setCreateTime(new Date());
        return account;
    }

    @Cacheable(value = "EHCACHE_A",key = "#account.id")
    public Account getAccountById(Account account) {
        Account account1 = new Account(account.getId(),"zhangfei","zf12345",new Date(),"张飞",2,false);
        return account1;
    }

    @Cacheable(value = "EHCACHE_A",key="#account.userName")
    public List<Account> getAccountList(Account account) {
        List<Account> accountList = Lists.newArrayList();
        accountList.add(new Account("124","zhaoyun","zy9999",new Date(),"赵云",3,true));
        accountList.add(new Account("125","lvbu","zy9999",new Date(),"吕布",3,true));
        System.out.println("查询accountList"+account);
        return accountList;
    }

    @CacheEvict(value = "EHCACHE_A",key="#account.id")
    public int deleteAccountById(Account account) {
        System.out.println("删除account"+account);
        return 1;
    }

    @CacheEvict(value = "EHCACHE_A",key = "#p0.id")
    public int updateAccountById(Account account) {
        System.out.println("更新account"+account);
        return 1;
    }
}
