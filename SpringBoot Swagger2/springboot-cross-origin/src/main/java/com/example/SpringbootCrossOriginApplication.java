package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/** 解决跨域问题
 * 1、注解驱动
 * 2、接口编程
 * 3、过滤器实现
 * 4、Actuator跨域
 */
@SpringBootApplication
public class SpringbootCrossOriginApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootCrossOriginApplication.class, args);
	}

}
