/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.filter;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * 创  建   时  间： 2020/5/5
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
@Log4j2
@SpringBootConfiguration // 将此Filter交给Spring容器管理
@WebFilter(urlPatterns = "/", filterName = "MyFilter2")
@Order(1)// 指定过滤器的执行顺序，值越大越靠后执行
public class MyFilter2 implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.info("过滤器 == MyFilter2 == init");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        log.info("过滤器 == MyFilter2 == before");
        filterChain.doFilter(servletRequest,servletResponse);
        log.info("过滤器 == MyFilter2 == after");
    }

    @Override
    public void destroy() {
        log.info("过滤器 == MyFilter2 == destroy");
    }
}
