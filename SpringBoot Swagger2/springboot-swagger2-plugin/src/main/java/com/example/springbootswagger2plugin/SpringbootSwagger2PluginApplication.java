package com.example.springbootswagger2plugin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootSwagger2PluginApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootSwagger2PluginApplication.class, args);
	}

}
