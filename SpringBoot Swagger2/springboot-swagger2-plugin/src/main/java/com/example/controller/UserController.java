/**
 * Copying (c) Yurian Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.controller;

import com.example.entity.UserInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

/**
 * 创  建   时  间： 2020/4/6
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 悠喃食品(深圳)有限公司
 */
@Api(description = "用户信息控制器",tags = {"用户操作接口1","用户操作接口2"})
@RestController
@RequestMapping("/user")
public class UserController {

    @GetMapping("/findUsers")
    @ApiOperation(value = "查询全部用户信息",tags = {"全部的","用户信息"},notes = "json格式")
    public List<UserInfo> findUsers(){

        return Collections.emptyList();
    }
}
