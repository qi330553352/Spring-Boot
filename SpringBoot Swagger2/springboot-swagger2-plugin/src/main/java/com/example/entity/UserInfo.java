/**
 * Copying (c) Yurian Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 创  建   时  间： 2020/4/6
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 悠喃食品(深圳)有限公司
 */
@Data
@ApiModel(description = "用户信息")
public class UserInfo implements Serializable {
    @ApiModelProperty(name = "id",notes = "用户ID")
    private Integer id;
    private String name;
    private Integer age;
    private Date createTime;
}
