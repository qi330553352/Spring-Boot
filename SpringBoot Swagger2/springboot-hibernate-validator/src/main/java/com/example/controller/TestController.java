/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.controller;

import com.example.entity.User;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * 创  建   时  间： 2020/5/5
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
@RestController
@Validated
public class TestController {
    @GetMapping("/hello")
    public String hello(@NotBlank(message = "{required}") String name,
                        @Email(message = "{invalid}") String email) {
        return "Hello World";
    }

    @GetMapping("/hello2")
    public String hello2(@Valid User user) {
        return "Hello World2";
    }

}
