package com.example.simple;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 创 建 时 间: 2019/10/15
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Component
public class Producer {

    @Value("${apache.rocketmq.producer.producerGroup}")
    private String producerGroup;
    @Value("${apache.rocketmq.namesrvAddr}")
    private String namesrvAddr;

    @PostConstruct
    public void defaultMQProducer() {
        // 生产者的组名
        DefaultMQProducer producer = new DefaultMQProducer(producerGroup);
        // 指定NameServer地址，多个地址以 ; 隔开
        producer.setNamesrvAddr(namesrvAddr);
        producer.setSendMsgTimeout(1000);
        producer.setRetryTimesWhenSendFailed(3);
        producer.setMaxMessageSize(1000);
        try {
            producer.start();
            for (int i = 0; i < 100; i++) {
                String messageBody = "发送给RocketMQ消息内容:" + i;
                String message = new String(messageBody.getBytes(), "utf-8");
                // 构建消息
                Message msg = new Message("PushTopic", "push", "key_" + i, message.getBytes());
                // 发送消息
                SendResult result = producer.send(msg);
                System.out.println("发送响应：MsgId:" + result.getMsgId() + "，发送状态:" + result.getSendStatus());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            producer.shutdown();
        }
    }
}
