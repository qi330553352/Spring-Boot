package com.example;

import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Service;

@SpringBootApplication
public class RocketmqPluginApplication {

	public static void main(String[] args) {
		SpringApplication.run(RocketmqPluginApplication.class, args);
	}


	// 声明消费消息的类，并在注解中指定，相关的消费信息
	@Service
	@RocketMQMessageListener(topic = "${spring.rocketmq.topic}", consumerGroup = "${spring.rocketmq.consumer.group}")
	class StringConsumer implements RocketMQListener<String> {
		@Override
		public void onMessage(String message) {
			System.out.printf("------- StringConsumer received: %s %f", message);
		}
	}
}
