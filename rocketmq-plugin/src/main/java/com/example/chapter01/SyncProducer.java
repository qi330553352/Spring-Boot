package com.example.chapter01;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;

/**
 * 创 建 时 间: 2019/10/19
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public class SyncProducer {
    public static void main(String[] args) throws Exception{
        DefaultMQProducer producer = new DefaultMQProducer("ExampleProducerGroup");
        producer.start();
        for (int i = 0; i < 100; i++) {
            byte[] str = ("Hello RocketMQ "+i).getBytes(RemotingHelper.DEFAULT_CHARSET);
            Message msg = new Message("TopicTest","TagA",str);
            producer.sendOneway(msg);
        }
        producer.shutdown();
    }
}
