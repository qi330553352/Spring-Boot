package com.example;

import com.example.filters.UrlFilter;
import com.example.filters.UrlFilter2;
import org.apache.catalina.filters.CorsFilter;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.mock.web.MockFilterConfig;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletContext;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.StaticWebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.FilterRegistration;
import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.SessionCookieConfig;
import javax.servlet.SessionTrackingMode;
import javax.servlet.descriptor.JspConfigDescriptor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.EventListener;
import java.util.Map;
import java.util.Set;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class SpringbootFilterApplicationTests {

	@Test
	void contextLoads() throws ServletException, IOException {
		WebApplicationContext webac = ContextLoader.getCurrentWebApplicationContext();
		System.out.println("webac:"+webac);
		if(webac!=null){
			ServletContext servletContext = webac.getServletContext();
		}
		//---------
		UrlFilter2 urlFilter2 = new UrlFilter2();
		urlFilter2.getFilterConfig();

		UrlFilter urlFilter = new UrlFilter();
		DelegatingFilterProxy filterProxy = new DelegatingFilterProxy(urlFilter);
		filterProxy.init(new MockFilterConfig(new MockServletContext()));
//		filterProxy.init(urlFilter2.getFilterConfig());


		MockHttpServletRequest request = new MockHttpServletRequest();
		MockHttpServletResponse response = new MockHttpServletResponse();
		filterProxy.doFilter(request, response, null);

	}

}
