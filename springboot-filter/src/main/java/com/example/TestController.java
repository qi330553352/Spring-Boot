/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example;

import com.example.filters.UrlFilter;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 创  建   时  间： 2020/4/15
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
@RestController
public class TestController {
    @RequestMapping("/hello")
    public String go(){
        return "go";
    }
    @RequestMapping("/gogo")
    public String gogo(){
        return "go";
    }
}
