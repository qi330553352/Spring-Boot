/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.configs;

import com.example.filters.UrlFilter;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

import javax.servlet.DispatcherType;
import java.util.ArrayList;
import java.util.List;

/**
 * 创  建   时  间： 2020/4/15
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
//@SpringBootConfiguration
public class WebConfiguration {

//    @Bean
    public FilterRegistrationBean<UrlFilter> tokenFilter() {
        FilterRegistrationBean<UrlFilter> filterReg = new FilterRegistrationBean<>(new UrlFilter());
        //优先级
        filterReg.setOrder(70);
        filterReg.setDispatcherTypes(DispatcherType.REQUEST);
        //匹配路径
        List<String> urlPatterns = new ArrayList<>();
//        urlPatterns.add("/*");
        filterReg.addUrlPatterns("/*"); // 过滤所有
//        filterReg.addInitParameter("exclusions","/gogo,/hello");
        filterReg.addInitParameter("exclusions","/gogo");
//        filterReg.setUrlPatterns(urlPatterns);
        System.out.println("====来了");
        return filterReg;
    }
}
