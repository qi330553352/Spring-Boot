package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ImportResource;

//@ServletComponentScan
@ImportResource({"classpath:applicationContext.xml"})
@SpringBootApplication
public class SpringbootFilterApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootFilterApplication.class, args);
	}

}
