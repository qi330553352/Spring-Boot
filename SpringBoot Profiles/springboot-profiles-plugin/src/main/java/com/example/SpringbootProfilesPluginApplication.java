package com.example;

import com.example.utils.ProfileUtil;
import com.example.vo.properties.OtherProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootProfilesPluginApplication implements CommandLineRunner {

	@Autowired
	private OtherProperties otherProperties;

	public static void main(String[] args) {
		SpringApplication.run(SpringbootProfilesPluginApplication.class, args);
	}

	@Override
	public void run(String... arg0) {
		System.out.println("\n" + otherProperties);
		System.out.println();
		System.out.println(ProfileUtil.getActiveProfile());
	}
}
