package com.example;

import com.example.configs.MyAuditorAware;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableJpaAuditing // 开启 JPA 的 Auditing 功能
@SpringBootApplication
@EnableTransactionManagement // 开启事务
public class SpringBootJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootJpaApplication.class, args);
	}

	@Bean
	public AuditorAware<Integer> auditorProvider() {
		return new MyAuditorAware();
	}
}
