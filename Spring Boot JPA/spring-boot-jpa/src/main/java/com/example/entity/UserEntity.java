/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * 创  建   时  间： 2020/9/19
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
@Data
@Entity
@Table(name = "user_customer", schema = "test", catalog = "")
public class UserEntity extends AbstractAuditable {
    @Column(name = "customer_name", nullable = true, length = 50)
    private String customerName;
    @Column(name = "customer_email", nullable = true, length = 50)
    private String customerEmail;
    //新增控制乐观锁的字段。并且加上@Version注解
    @Version
    @Column(name = "version", nullable = true)
    private Long version;
}
