/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.repository;

import com.example.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.function.Predicate;

/**
 * 创  建   时  间： 2020/9/18
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
public interface UserQueryDSLRepository extends CrudRepository<User, Long>, QuerydslPredicateExecutor<User> {

    Page<User> findAll(Predicate predicate, Pageable pageable);
}
