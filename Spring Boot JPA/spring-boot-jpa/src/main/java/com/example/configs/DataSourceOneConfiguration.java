/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.configs;

import org.hibernate.jpa.boot.spi.EntityManagerFactoryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import java.util.Map;

/**
 * 多数据源
 我们发现 DataSourceTwoConfig、DataSourceOneConfig 内容基本一样，思路就是管理两套 datasource，
 从而带来了两套 transactionManager，分别在这两个 package 下创建各自的实体和数据访问接口即可。
 当然了也可以通过 @Transactional(rollbackFor = Exception.class, transactionManager= "transactionManagerOne")
 来手动选择哪个数据源。
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef="entityManagerFactoryOne",
        transactionManagerRef="transactionManagerOne",
        basePackages= { "com.jackzhang.example.one" }) //设置Repository所在位置
@EnableConfigurationProperties(JpaProperties.class)
public class DataSourceOneConfiguration {
    /**
     * 配置数据源1
     */
    @Primary
    @Bean(name = "dataSourceOne")
    @ConfigurationProperties("spring.datasource.druid.one")
    public DataSource dataSourceOne(){
//        return DruidDataSourceBuilder.create().build();
        return null;
    }

    @Autowired
    @Qualifier("dataSourceOne")
    private DataSource oneDataSource;
    @Primary
    @Bean(name = "entityManagerOne")
    public EntityManager entityManager(EntityManagerFactoryBuilder builder) {
        return entityManagerFactoryOne(builder).getObject().createEntityManager();
    }
    @Primary
    @Bean(name = "entityManagerFactoryOne")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryOne (EntityManagerFactoryBuilder builder) {
//        return builder
//                .dataSource(oneDataSource)
//                .properties(getVendorProperties(oneDataSource))
//                .packages("com.jackzhang.example.one") //设置实体类所在位置
//                .persistenceUnit("onePersistenceUnit")
//                .build();
        return null;
    }
    @Autowired
    private JpaProperties jpaProperties;
    private Map<String, String> getVendorProperties(DataSource dataSource) {
//        return jpaProperties.getHibernateProperties(dataSource);
        return null;
    }
    @Primary
    @Bean(name = "transactionManagerOne")
    public PlatformTransactionManager transactionManagerOne(EntityManagerFactoryBuilder builder) {
        return new JpaTransactionManager(entityManagerFactoryOne(builder).getObject());
    }
}
