/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.configs;

import org.springframework.data.domain.AuditorAware;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Optional;

/**
 * 创  建   时  间： 2020/9/19
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
public class MyAuditorAware implements AuditorAware<Integer> {
    /**
     * Returns the current auditor of the application.
     * @return the current auditor
     */
    @Override
    public Optional<Integer> getCurrentAuditor() {
//    第一种方式：如果我们集成了spring的Security，我们直接通过如下方法即可获得当前请求的用户ID.
//    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//    if (authentication == null || !authentication.isAuthenticated()) {
//       return null;
//    }
//    return ((LoginUserInfo) authentication.getPrincipal()).getUser().getId();
        //第二种方式通过request里面取或者session里面取
        ServletRequestAttributes servletRequestAttributes =
                (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        return (Optional<Integer>) servletRequestAttributes.getRequest().getSession().getAttribute("userId");
    }
}
