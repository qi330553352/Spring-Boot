/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.configs;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;

import java.util.Objects;

/**
 * 自定义Naming 命名策略
 * 创  建   时  间： 2020/9/19
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
public class MyPhysicalNamingStrategy extends PhysicalNamingStrategyStandardImpl {
    //重载PhysicalColumnName方法，修改字段的物理名称。
    @Override
    public Identifier toPhysicalColumnName(Identifier name, JdbcEnvironment context) {
        String text = warp(name.getText());
        if(Objects.equals(text.charAt(0) , '_')){
            text = text.replaceFirst("_","");
        }
        return super.toPhysicalColumnName(new Identifier(text,name.isQuoted()), context);
    }
    //将驼峰式命名转化成下划线分割的形式
    public static String warp(String text){
        text = text.replaceAll("([A-Z])","_$1").toLowerCase();
        if(Objects.equals(text.charAt(0) , '_')){
            text = text.replaceFirst("_","");
        }
        return text;
    }
}
