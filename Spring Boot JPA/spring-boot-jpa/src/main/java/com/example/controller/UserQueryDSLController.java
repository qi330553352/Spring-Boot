/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.controller;

import com.example.entity.QUser;
import com.example.entity.User;
import com.example.repository.UserQueryDSLRepository;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.function.Predicate;

/**
 * 创  建   时  间： 2020/9/18
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
@RestController
@RequestMapping("query/dsl/")
public class UserQueryDSLController {
    @Autowired
    private UserQueryDSLRepository userQueryDSLRepository;

    @GetMapping("users")
    public Page<User> findByParam(@QuerydslPredicate(root = User.class) Predicate predicate, Pageable pageable) {
        return userQueryDSLRepository.findAll(predicate,pageable);
    }

    @GetMapping("users/all")
    public Iterable<User> find() {
        QUser user = QUser.user;
        //直接引用QUser通过下面的操作直接做查询
        Predicate predicate = (Predicate) user.name.startsWith("jack")
                .and(user.email.startsWithIgnoreCase("jack"));
        return userQueryDSLRepository.findAll((OrderSpecifier<?>) predicate);
    }
}
