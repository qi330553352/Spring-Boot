package com.example.utils;

import com.example.MyTable;
import com.example.User;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;

/**
 * 创  建   时  间： 2020/3/8
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 悠喃食品(深圳)有限公司
 */
public enum BeanUtil {
    INSTANCE;
    private InnerClass _instance;
    private BeanUtil(){
        _instance = new InnerClass();
    }
    public InnerClass getInstance(){
        return _instance;
    }
    public static class InnerClass{
    }
}
